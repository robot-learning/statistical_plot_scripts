import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tf

class errorAPI:
    def __init(self):
        print 'initialized errorAPI'

    
    # compute orientation err: q_des is the desired quaternion(qx,qy,qz,qw), q_reach is the the reached/planned quaternion
    def quat_norm(self,q_des,q_reach):
        if(len(q_des)!=4):
            q_des = tf.transformations.quaternion_from_euler(q_des[0],q_des[1],q_des[2])

        if(len(q_reach)!=4):
            q_reach = tf.transformations.quaternion_from_euler(q_reach[0],q_reach[1],q_reach[2])

        q_sum=np.linalg.norm(q_des+q_reach,ord=2)
        q_diff=np.linalg.norm(q_des-q_reach,ord=2)
        R_err=np.min([q_sum,q_diff])/np.sqrt(2.0) *100.0
        return R_err

    # compute position error and orientation error: d_pose is the desired pose (x,y,z,qx,qy,qz,qw) and r_pose is the reached/planned pose 
    def pose_err(self,d_pose,r_pose):
        # compute position err:
        position_err=np.linalg.norm(np.ravel(d_pose[0:3]-r_pose[0:3]))
        # orientation error:
        or_err=self.quat_norm(d_pose[3:],r_pose[3:])
        return [position_err,or_err]

    # Compute the position error percentage: d_pose is the desired pose, i_pose is the initial pose and r_pose is the reached/planned pose
    def position_error_perc(self,d_pose,i_pose,r_pose):
        err = 100.0 * np.linalg.norm(d_pose[0:3] - r_pose[0:3])/np.linalg.norm(d_pose[0:3]-i_pose[0:3])
        return err
