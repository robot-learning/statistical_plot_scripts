# This file takes mean, variance and plots them as box plot using seaborn
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from data_api import *
import seaborn as sns




def get_data():
    err_p=[] # position error
    err_o=[] # orientation err
    meth_arr=[] # method of in-hand manipulation
    obj_arr=[] # object name
                           
    for i in range(100):
        # replace with actual error
        p_err = np.random.rand()
        or_err = np.random.rand()
        err_p.append(p_err*1.0)
        err_o.append(or_err*1.0)

        # store method and object name
        meth_arr.append('Relaxed-Rigidity')
        obj_arr.append('Apple')


        # repeat for other methods
        p_err = np.random.rand()
        or_err = np.random.rand()
        err_p.append(p_err*1.0)
        err_o.append(or_err*1.0)
        meth_arr.append('Point-Contact')
        obj_arr.append('Apple')


    # create pandas dataframe
    df_new = pd.DataFrame()
    df_new['Object'] = obj_arr
    df_new['Method'] = meth_arr
    df_new['p_err'] = err_p
    df_new['o_err'] = err_o
    
    # return data
    return df_new

def box_plot(data_):
    
    ax=sns.boxplot(x="Object", y="p_err",hue='Method',whis = 1.5,data=data_)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels,title='',loc='upper center', bbox_to_anchor=(0.5, 1.11),ncol=5, fancybox=False, shadow=False)
    plt.show()
if __name__=='__main__':
    # 
    d = get_data()
    box_plot(d)
